#+Title: Server Side Rendering with CLJS
#+Date: 03.08.2019
#+Author: Steven Thonus
#+Email: steven@ln2.nl

#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:t reveal_control:t
#+OPTIONS: reveal_mathjax:t reveal_rolling_links:t reveal_keyboard:t reveal_overview:t
#+OPTIONS: num:nil toc:nil
#+OPTIONS: author:t date:f email:t

#+REVEAL_TRANS: linear
#+REVEAL_THEME: white
#+REVEAL_HLEVEL: 999
#+OPTIONS: reveal_title_slide:"<h1>%t</h1><p>%a</p>"
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_EXTRA_CSS: ./style.css


* Demo Singe Page Application
#+BEGIN_NOTES
- http://localhost:4200
- Static page served by node server
- Api on node server
- Classic single page application
- Show network tab
- Disable javascript
#+END_NOTES

* Missing features
#+ATTR_REVEAL: :frag (appear)
- Needs javascript
- Initial page is empty
- Only single OpenGraph tag usable

* Server Side Rendering
#+ATTR_REVEAL: :frag (appear)
- Render page on server
- Use (mostly) the same code to do this
- Mind routing, cookies, localstorage, authentication...!
- Determine when rendering is done
- Transfer html and current state to client
- Show and hydrate the app

* SSR test project
#+ATTR_REVEAL: :frag (appear)
- Shadow-cljs compiler for browser and Node.js
- Node server for static files and API
- Hot code reloading

* Bamse
#+attr_html: :width 40%
#+attr_latex: :width 40%
[[./Bamse.jpg]]
#+BEGIN_NOTES
http://localhost:9630
http://localhost:3000
#+END_NOTES

* Goal
- make SSR easy for CLJS
- use SSR in a project
- receive feedback, tips, questions

* About
#+BEGIN_CENTER
#+REVEAL_HTML: <img class="avatar" src="avatar.png">
*Steven Thonus*\\
steven@ln2.nl\\
\\
https://gitlab.com/StevenT/bamse\\
\\
https://stevent.gitlab.io/heart-of-clojure-2019-lightning-talk-ssr
#+END_CENTER
